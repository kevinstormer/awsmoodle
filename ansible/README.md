
= Setup

To install ansible and the prereqs you need without polluting your system python with packages, you can use a virtual environment:
```
% python3 -m virtualenv venv
% source venv/bin/activate
(venv) % pip install -r requirements.txt
(venv) % ansible-galaxy install -r requirements.yml
```

Otherwise, if you've installed ansible with your package manger:
```
% cd /path/to/ansible
% ansible-galaxy install -r requirements.yml
```

= Vault

To encrypt a string that you can drop into a playbook or variables file:
`ansible-vault encrypt_string password123 --ask-vault-pass`