# Set here a list of allowed IP addresses that will have "admin" access to your moodle farm
output "allowlisted_ips" {
  value = [
    "xxx.xxx.xxx.xxx/25", # allowed IPs
    "xxx.xxx.xxx.xxx/32",   # monitoring server
  ]
}

output "vpc_cidrs" {
  description = "central lookup for VPC cidrs"
  # Entries are orderd by their CIDR values
  value = {
    vpc1  = "10.43.0.0/16"
  }
}

output "amis" {
  value = {
    us-east-2 = {
      amazonlinux2 = "ami-0f7919c33c90f5b58"
      centos7 = "ami-01e36b7901e884a10"
    }
  }
}
