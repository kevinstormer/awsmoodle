# Change the domain to match your needed Moodle domain name
locals {
  common_tags = {
    automation          = "terraform"
    "automation.config" = "hanover/moodle_prod"
  }
  environment = "moodle-prod"
  domain = ["moodle-prod.example.com","moodle.example.com"]
}
