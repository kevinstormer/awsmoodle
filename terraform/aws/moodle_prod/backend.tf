# Choose region to match your current location
terraform {
  backend "s3" {
    region  = "us-east-2"
    bucket  = "hanover-terra-20200622"
    key     = "hanover/moodle_prod.json"
    encrypt = true
  }
}

# Choose the corresponding region that makes sense to your location
provider "aws" {
  region = "us-east-2"
}

module "global_variables" {
  source = "../../../modules/global_variables"
}

data "aws_region" "current" {}

data "terraform_remote_state" "shared" {
  backend = "s3"

  config = {
    region  = "us-east-2"
    bucket  = "hanover-terra-20200622"
    key     = "hanover/shared.json"
    encrypt = true
  }
}
