# Elasticache creation. You might find you don't need it but we saw a significant speed increase with
# using a cache server with our Moodle instance.
# Change node_type if need a larger cache server
resource "aws_elasticache_cluster" "redis" {
  cluster_id           = local.environment
  node_type            = "cache.t3.micro"
  num_cache_nodes      = 1
  engine               = "redis"
  engine_version       = "5.0.6"
  parameter_group_name = "default.redis5.0"
  port                 = 6379
  subnet_group_name    = aws_elasticache_subnet_group.redis.name
  security_group_ids   = [aws_security_group.redis.id]
}

resource "aws_elasticache_subnet_group" "redis" {
  name       = "${local.environment}-redis"
  subnet_ids = data.terraform_remote_state.shared.outputs.private_subnets
}

resource "aws_security_group" "redis" {
  name        = "${local.environment}-redis"
  description = "${local.environment}-redis"
  vpc_id      = data.terraform_remote_state.shared.outputs.vpc_id

  ingress {
    from_port       = "6379"
    to_port         = "6379"
    protocol        = "tcp"
    security_groups = [aws_security_group.moodle.id]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
