# This file creates an elastic file system for Moodle data directory. This directory can grow quickly
# depending on what content you want to host on your Moodle instance. This is also a shared storage
# between your multiple Moodle servers
resource "aws_efs_file_system" "commonstorage" {

  tags = merge(local.common_tags, { "Name" = "${local.environment}-commonstorage" })

  lifecycle_policy {
    transition_to_ia = "AFTER_7_DAYS"
  }

}

resource "aws_efs_mount_target" "commonstorage" {
  count = length(data.terraform_remote_state.shared.outputs.private_subnets)

  file_system_id  = aws_efs_file_system.commonstorage.id
  subnet_id       = data.terraform_remote_state.shared.outputs.private_subnets[count.index]
  security_groups = [aws_security_group.commonstorage.id]

}

resource "aws_efs_file_system" "moodlewebstorage" {

  tags = merge(local.common_tags, { "Name" = "${local.environment}-moodlewebstorage" })

  lifecycle_policy {
    transition_to_ia = "AFTER_7_DAYS"
  }

}

resource "aws_efs_mount_target" "moodlewebstorage" {
  count = length(data.terraform_remote_state.shared.outputs.private_subnets)

  file_system_id  = aws_efs_file_system.moodlewebstorage.id
  subnet_id       = data.terraform_remote_state.shared.outputs.private_subnets[count.index]
  security_groups = [aws_security_group.commonstorage.id]

}


resource "aws_security_group" "commonstorage" {
  name        = "${local.environment}-commonstorage"
  description = "Access to efs for moodle instances"
  vpc_id      = data.terraform_remote_state.shared.outputs.vpc_id

  ingress {
    from_port       = "2049"
    to_port         = "2049"
    protocol        = "tcp"
    security_groups = [aws_security_group.moodle.id]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
