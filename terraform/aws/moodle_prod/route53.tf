# Add DNS entries in AWS route53. If you don't use AWS Route53 for DNS you would need to add these
# entries manually on your DNS server.
resource "aws_route53_record" "moodle" {
  name    = "moodle-prod"
  type    = "CNAME"
  zone_id = data.aws_route53_zone.hanover_edu_zone.id
  records = [aws_lb.moodle.dns_name]
  ttl     = 300
}

resource "aws_route53_record" "moodle_server" {
  count = length(aws_instance.moodle)
    
  name    = "${local.environment}${count.index}"
  type    = "CNAME"
  zone_id = data.aws_route53_zone.hanover_edu_zone.id
  records = [aws_instance.moodle[count.index].public_ip]
  ttl     = 300
}