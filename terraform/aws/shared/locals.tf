# Change availability zones to match your location + AWS region
locals {
  common_tags = {
    automation          = "terraform"
    "automation.config" = "hanover/shared"
  }
  availability_zones = ["us-east-2a", "us-east-2b", "us-east-2c"]
  environment        = "shared"
}
