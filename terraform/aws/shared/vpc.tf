module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.9.0"

  name                 = "vpc1"
  cidr                 = module.global_variables.vpc_cidrs.vpc1
  azs                  = local.availability_zones
  private_subnets      = ["10.43.1.0/24", "10.43.2.0/24", "10.43.3.0/24"]
  public_subnets       = ["10.43.4.0/24", "10.43.5.0/24", "10.43.6.0/24"]
  database_subnets     = ["10.43.7.0/24", "10.43.8.0/24", "10.43.9.0/24"]
  enable_dns_hostnames = true
  enable_nat_gateway   = true
  single_nat_gateway   = true

  tags = local.common_tags
}