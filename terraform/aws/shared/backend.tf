# Change AWS region to match what you need
terraform {
  backend "s3" {
    region   = "us-east-2"
    bucket   = "hanover-terra-20200622"
    key      = "hanover/shared.json"
    encrypt  = true
#    role_arn = "arn:aws:iam::846359942316:role/terraform-servicerole"
  }
}

module "global_variables" {
  source = "../../../modules/global_variables"
}

provider "aws" {
  region = "us-east-2"

  # assume_role {
  #   role_arn = "arn:aws:iam::846359942316:role/terraform-servicerole"
  # }
}
