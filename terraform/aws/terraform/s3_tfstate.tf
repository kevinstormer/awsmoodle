
resource "aws_s3_bucket" "tfstate" {
  bucket = "hanover-terra-20200622"
  versioning {
    enabled = true
  }
  tags = local.common_tags
}
