locals {
  common_tags = {
    automation          = "terraform"
    "automation.config" = "hanover/terraform"
    caution             = "critical_terraform_component"
  }
}
