# Another place to change region to match your location
terraform {
  backend "s3" {
    region   = "us-east-2"
    bucket   = "hanover-terra-20200622"
    key      = "hanover/terraform.json"
    encrypt  = true
    # role_arn = "arn:aws:iam::288328080531:role/terraform-servicerole"
  }
}

module "global_variables" {
  source = "../../../modules/global_variables"
}

provider "aws" {
  region = "us-east-2"

  # assume_role {
    # role_arn = "arn:aws:iam::288328080531:role/terraform-servicerole"
  # }
}
