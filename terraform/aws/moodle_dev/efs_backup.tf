
# Change following backup plan to meet your requirements for backup
resource "aws_backup_plan" "efs_backup" {
  name = "${local.environment}-efs-backup"

# daily backup every day at 2am
  rule {
    rule_name         = "daily_rule"
    target_vault_name = aws_backup_vault.efs_backup.name
    schedule          = "cron(0 2 * * * *)"
    lifecycle {
      delete_after = 90
    }
  }

# weekly backup every Sunday at 4am
  rule {
    rule_name         = "weekly_rule"
    target_vault_name = aws_backup_vault.efs_backup.name
    schedule          = "cron(0 4 * * SUN *)"
    lifecycle {
      delete_after = 90
    }
  }

# yearly backup on 1 August at 5:30am (before school starts)
  rule {
    rule_name         = "yearly_rule"
    target_vault_name = aws_backup_vault.efs_backup.name
    schedule          = "cron(30 5 1 8 * *)"
    lifecycle {
      delete_after = 365
    }
  }
}

resource "aws_backup_vault" "efs_backup" {
  name = "${local.environment}-efs-backup"
}

resource "aws_backup_selection" "efs_backup" {
  name         = "${local.environment}-efs-backup"
  plan_id      = aws_backup_plan.efs_backup.id
  iam_role_arn = aws_iam_role.efs_backup.arn

  resources = [
    aws_efs_file_system.commonstorage.arn
  ]
}


resource "aws_iam_role" "efs_backup" {
  name               = "${local.environment}-efs-backup"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": ["sts:AssumeRole"],
      "Effect": "allow",
      "Principal": {
        "Service": ["backup.amazonaws.com"]
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "efs_backup" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.efs_backup.name
}
