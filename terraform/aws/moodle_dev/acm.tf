resource "aws_acm_certificate" "moodle" {
  domain_name               = local.domain
  validation_method         = "DNS"
}

# sets up DNS records in AWS route53 for certificate validation 
# Needs to be performed manually if route53 not already setup
resource "aws_route53_record" "moodle" {
  name    = tolist(aws_acm_certificate.moodle.domain_validation_options)[0].resource_record_name
  type    = tolist(aws_acm_certificate.moodle.domain_validation_options)[0].resource_record_type
  zone_id = data.aws_route53_zone.hanover_edu_zone.id
  records = [tolist(aws_acm_certificate.moodle.domain_validation_options)[0].resource_record_value]
  ttl     = 300
}

resource "aws_acm_certificate_validation" "moodle" {
  certificate_arn         = aws_acm_certificate.moodle.arn
  validation_record_fqdns = [aws_route53_record.moodle.fqdn]
}