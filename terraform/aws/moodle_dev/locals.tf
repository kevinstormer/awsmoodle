# Change the domain to match your needed Moodle domain name
locals {
  common_tags = {
    automation          = "terraform"
    "automation.config" = "hanover/moodle_dev"
  }
  environment = "moodle-dev"
  domain = "moodle-dev.example.com"
}
