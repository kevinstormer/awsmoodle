
# Change instance type and count to server computing size that you need 
# and how many servers you need (do not go below 2 to keep redundancy)
resource "aws_instance" "moodle" {
  count = 2
  instance_type = "t3.small"
  key_name      = data.terraform_remote_state.shared.outputs.default_moodle_key
  ami           = module.global_variables.amis[data.aws_region.current.name]["centos7"]
  subnet_id     = data.terraform_remote_state.shared.outputs.public_subnets[0]

  vpc_security_group_ids = [
    aws_security_group.moodle.id
  ]

  volume_tags = merge(
    local.common_tags,
    {
      "Name" = "${local.environment}${count.index}"
    },
  )
  tags        = merge(
    local.common_tags,
    {
      "Name" = "${local.environment}${count.index}"
    },
  )
}


resource "aws_security_group" "moodle" {
  name        = local.environment
  description = local.environment
  vpc_id      = data.terraform_remote_state.shared.outputs.vpc_id

  ingress {
    from_port       = "80"
    to_port         = "80"
    protocol        = "tcp"
    security_groups = [aws_security_group.lb_moodle.id]
    cidr_blocks = concat(
      module.global_variables.allowlisted_ips
    )
  }

  ingress {
    from_port = "443"
    to_port   = "443"
    protocol  = "tcp"
    cidr_blocks = concat(
      module.global_variables.allowlisted_ips
    )
  }

#Allow SSH on port 22 from a local Moodle server if scp needed.
  ingress {
    from_port = "22"
    to_port   = "22"
    protocol  = "tcp"
    cidr_blocks = concat(
      module.global_variables.allowlisted_ips,
      ["xxx.xxx.xxx.xxx/32"] # local Moodle IP
    )
  }

  ingress {
    from_port = 8
    to_port = 0
    protocol = "icmp"
    cidr_blocks = concat(
      module.global_variables.allowlisted_ips
    )
  }

  egress {
    from_port        = "0"
    to_port          = "0"
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}