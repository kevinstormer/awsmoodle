# File to create database for Moodle. This is shared database between multiple Moodle servers.
resource "random_password" "moodle" {
  length  = 24
  special = false
}


resource "aws_security_group" "db_moodle" {
  name        = "db-moodle-${local.environment}"
  description = "Group for Moodle DB"
  vpc_id      = data.terraform_remote_state.shared.outputs.vpc_id

  ingress {
    from_port = "3306"
    to_port   = "3306"
    protocol  = "tcp"
    security_groups = [aws_security_group.moodle.id]
    cidr_blocks = concat(
      data.terraform_remote_state.shared.outputs.private_subnets_cidr_blocks,
      module.global_variables.allowlisted_ips
    )
  }

  egress {
    from_port        = "0"
    to_port          = "0"
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

# Change instance class to change size of database server
module "moodle_rds" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier                      = "moodle-rds-${local.environment}"
  engine                          = "mysql"
  engine_version                  = "5.7"
  family                          = "mysql5.7"
  major_engine_version            = "5.7"
  instance_class                  = "db.t3.micro"
  allocated_storage               = 5
  max_allocated_storage           = 1024
  storage_encrypted               = false
  vpc_security_group_ids          = [aws_security_group.moodle_rds.id]
  subnet_ids                      = data.terraform_remote_state.shared.outputs.database_subnets
  name                            = "moodle"
  username                        = "dbadmin"
  password                        = random_password.moodle.result
  port                            = "3306"
  maintenance_window              = "Mon:06:00-Mon:07:00"
  backup_window                   = "03:00-06:00"
  backup_retention_period         = 1
  enabled_cloudwatch_logs_exports = ["error", "general", "slowquery", ]

  tags = local.common_tags
}

resource "aws_security_group" "moodle_rds" {
  name        = "db-moodle-rds-${local.environment}"
  description = "Group for Moodle RDS DB"
  vpc_id      =  data.terraform_remote_state.shared.outputs.vpc_id

  ingress {
    from_port = "3306"
    to_port   = "3306"
    protocol  = "tcp"
    security_groups = [aws_security_group.moodle.id]
    cidr_blocks = concat(
      data.terraform_remote_state.shared.outputs.private_subnets_cidr_blocks,
      module.global_variables.allowlisted_ips
    )
  }

  egress {
    from_port        = "0"
    to_port          = "0"
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}