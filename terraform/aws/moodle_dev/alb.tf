resource "aws_lb" "moodle" {
  name               = local.environment
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_moodle.id]
  subnets            = data.terraform_remote_state.shared.outputs.public_subnets

  tags = local.common_tags
}

resource "aws_lb_listener" "moodle" {
  load_balancer_arn = aws_lb.moodle.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.moodle.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.moodle.arn
  }
}

resource "aws_lb_listener" "moodle_http" {
  load_balancer_arn = aws_lb.moodle.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_target_group" "moodle" {
  name                 = local.environment
  vpc_id               = data.terraform_remote_state.shared.outputs.vpc_id
  target_type          = "ip"
  protocol             = "HTTP"
  port                 = 80
  deregistration_delay = 120

  health_check {
    enabled  = true
    path     = "/"
    port     = 80
    protocol = "HTTP"
    matcher = "303"
  }
}

resource "aws_lb_target_group_attachment" "moodle" {
  count = length(aws_instance.moodle)
  target_group_arn = aws_lb_target_group.moodle.arn
  target_id        = aws_instance.moodle[count.index].private_ip
  port             = 80
}

resource "aws_security_group" "lb_moodle" {
  name        = "lb-${local.environment}"
  description = "lb-${local.environment}"
  vpc_id      = data.terraform_remote_state.shared.outputs.vpc_id

  ingress {
    from_port        = "443"
    to_port          = "443"
    protocol         = "tcp"
    cidr_blocks      = module.global_variables.allowlisted_ips
    ipv6_cidr_blocks = []
  }

  ingress {
    from_port        = "80"
    to_port          = "80"
    protocol         = "tcp"
    cidr_blocks      = module.global_variables.allowlisted_ips
    ipv6_cidr_blocks = []
  }

  egress {
    from_port        = "0"
    to_port          = "0"
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
